import json
from subprocess import check_call
import os

class Kern():
    kerns = {}

    def load(self):
        try:
            with open('db/kernels.json', 'r') as json_file:
                self.kerns = json.load(json_file)
        except:
            # file/dir doesn't exist, we'll fix that when we save it.
            self.kerns = {}

    def save(self):
        try:
            with open('db/kernels.json', 'w') as json_file:
                json.dump(self.kerns, json_file, indent=2)
        except:
            os.mkdir('db')
            with open('db/kernels.json', 'w+') as json_file:
                json.dump(self.kerns, json_file, indent=2)

    def dump_kerns(self):
        return self.kerns.items()

    def add_kern(self, version, tarball, workspace):
        self.kerns[version] = {
            'tarball': tarball,
            'workspace': workspace,
            'complete': False
            }
        self.save()

    def mark_complete(self, version):
        self.kerns[version]['complete']=True
        self.save()

    def mark_incomplete(self, version):
        self.kerns[version]['complete']=False
        self.save()

    def prep_data(self, version):
        self.kerns[version]['artifacts'] = {
                'job_id': os.getenv('CI_JOB_ID'),
                'pipeline_id': os.getenv('CI_PIPELINE_ID'),
            }
        self.save()

    def commit(self):
        check_call(['git', 'add', 'db/*'])
        check_call(['git', 'commit', '-m', '[CI: $CI_PIPELINE_ID] Built Kernels.'])
        check_call(['git', 'push'])

    def prep_git(self):
        check_call(['git', 'remote', 'set-url', 'origin', 'https://kebrx:' + os.getenv('GL_ACCESS_TOKEN') + '@gitlab.com/kebrx/acs-override.git'])
        check_call(['git', 'config', '--global', 'user.name', 'Kyle Brennan'])
        check_call(['git', 'config', '--global', 'user.email', 'kyle@kebrx.tech'])
        check_call(['git', 'remote', '-v'])
        check_call(['git', 'checkout', os.getenv('CI_COMMIT_REF_NAME')])
        check_call(['git', 'pull', '--rebase', 'origin', 'master'])
