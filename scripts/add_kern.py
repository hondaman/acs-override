import contextlib
import os
import argparse
import kern

def main(args):
    kernel = kern.Kern()
    kernel.load()
    kernel.add_kern(args.version, args.tarball, args.workspace)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='add another kernel job to run')
    parser.add_argument('version', help='kernel version to use')
    parser.add_argument('tarball', help='tarball URL')
    parser.add_argument('workspace', help='which workspace to use')
    args = parser.parse_args()
    main(args)
