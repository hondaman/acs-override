import contextlib
import os
from subprocess import check_call, check_output
import kern
import multiprocessing
import time
import json
import argparse

@contextlib.contextmanager
def pushd(path):
    prev_cwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_cwd)

def main():
    kernels = kern.Kern()
    kernels.load()
    kernels.prep_git()
    key = os.getenv('KERNEL_BUILD')
    kernel = kernels.kerns[key]
    if not kernel['complete']:
        print('incomplete: ', key)
        with pushd('workspaces/'+kernel['workspace']):
            print('downloading', kernel['tarball'])
            check_call(['curl', kernel['tarball'], '--output', 'linux.tar.xz'])
            os.mkdir('linux')
            check_call(['tar', 'xf', 'linux.tar.xz', '-C', 'linux', '--strip-components=1'])
            os.remove('linux.tar.xz')
            with pushd('linux'):
                # copy config in place.
                try:
                    check_call(['cp', '../kernel_config', '.config'])
                except:
                    print('No Kernel config available, using default.')
                for filename in os.listdir('..'):
                    if filename.endswith('.patch'):
                        print('Patching:', filename)
                        check_call(['patch', '-p', '1', '-i', '../'+filename])
                check_call(['make', 'olddefconfig'])
                check_call(['make', '-j', str(multiprocessing.cpu_count()), 'deb-pkg', 'LOCALVERSION=-acso'])

        # copy to TLD:
        kernels.mark_complete(key)
        kernels.prep_data(key)
        kernels.commit()

def manage():
    kernels = kern.Kern()
    kernels.load()
    for key, kernel in kernels.kerns.items():
        if not kernel['complete']:
            print('Launching Build for: '+ key)
            launch_build = check_output(['curl', '-X', 'POST', '-F', 'token='+os.getenv('TRIGGER_TOKEN'), '-F', 'ref=master', '-F', 'variables[BUILD_KERNEL]=true', '-F', 'variables[KERNEL_BUILD]='+key, 'https://gitlab.com/api/v4/projects/17194730/trigger/pipeline'])
            launch_json = json.loads(launch_build)
            print('Build Launched, id: ' + str(launch_json['id']))
            while True:
                output = json.loads(check_output(['curl', '--header', 'PRIVATE-TOKEN: '+os.getenv('GL_ACCESS_TOKEN'), 'https://gitlab.com/api/v4/projects/17194730/pipelines/'+str(launch_json['id'])]))

                if output['status']=="success":
                    print('on to the next!')
                    break
                elif output['status']=="failed":
                    print('something went wrong!')
                    exit(1)
                else:
                    print('waiting another 10 sec')
                    time.sleep(10)

def clean():
    kernel = kern.Kern()
    kernel.load()
    for key, kernel in kernel.kerns.items():
        if not kernel['complete']:
            print('cleaning up, ', key, '| workspaces/'+kernel['workspace'])
            with pushd('workspaces/'+kernel['workspace']):

                check_call(['rm', '-rf', 'linux'])
                try:
                    os.remove('linux.tar.gz')
                except:
                    pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Auto patch and build kernel')
    parser.add_argument('--clean', action='store_true', help='Clean out all the workspaces')
    parser.add_argument('--manage', action='store_true', help='manage multiple builds')
    args = parser.parse_args()
    if args.clean:
        clean()
    elif args.manage:
        manage()
    else:
         main()
