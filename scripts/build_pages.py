import kern
import os

def append_file(filename, data):
    try:
        with open(filename, 'w') as fobj:
            fobj.write(data)
    except:
        try:
            os.mkdir('public')
        except:
            pass
        with open(filename, 'w+') as fobj:
            fobj.write(data)

def build_card(key,knl):
    jobid = str(knl['artifacts']['job_id'])
    version = key

    return '''
    <div class="card" style="width: 18rem;">
    <h3 class="card-header">''' + version + '''</h3>
    <div class="list-group list-group-flush">
    <a class="list-group-item list-group-item-action text-primary" href="https://gitlab.com/kebrx/acs-override/-/jobs/''' + jobid + '''/artifacts/raw/linux-image-''' + version + '''-acso_''' + version + '''-acso-1_amd64.deb">Image</a>
    <a class="list-group-item list-group-item-action text-primary" href="https://gitlab.com/kebrx/acs-override/-/jobs/''' + jobid + '''/artifacts/raw/linux-headers-''' + version + '''-acso_''' + version + '''-acso-1_amd64.deb">Headers</a>
    <a class="list-group-item list-group-item-action text-primary" href="https://gitlab.com/kebrx/acs-override/-/jobs/''' + jobid + '''/artifacts/raw/linux-''' + version + '''-acso_''' + version + '''-acso.orig.tar.gz">Source</a>
    </div>
    <div class="card-body">
    <div class="btn-group btn-group-sm">
    <a href="https://gitlab.com/kebrx/acs-override/-/jobs/''' + jobid + '''/artifacts/download" class="btn btn-outline-primary">Download All</a>
    <a href="https://gitlab.com/kebrx/acs-override/-/jobs/''' + jobid + '''/artifacts/browse" class="btn btn-outline-secondary">Browse</a>
    <a href="https://gitlab.com/kebrx/acs-override/raw/master/workspaces/''' + jobid + '''/acso.patch" class="btn btn-outline-secondary">Patch</a>
    </div>
    </div>
    </div>
    '''

def main():
    contents = '''
    <html>
    <head>
    <title>Linux Kernel ACS Builds</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body style="margin: 1rem;">
    <div >
    <div class="jumbotron">
    <h1>ACS Kernel Builds</h1>
    <p class="lead">This page contains links to the latest kernel builds with ACS override patches applied, built by yours truly.</p>
    </div>
    <div class="alert alert-warning">This project is still early beta.  Expect errors.</div>
    <div class="card-deck">
    '''
    kernels = kern.Kern()
    kernels.load()
    iterator = 0
    for key, kernel in kernels.kerns.items():
        print('formatting: ', key)
        if kernel['complete']:
            if iterator ==5:
                iterator=0
                contents += '''
                </div>
                <div class="card-deck">
                '''
            iterator+=1
            contents += build_card(key,kernel)

    contents += '''
    </div>
    </div>
    </body>
    </html>
    '''

    append_file('public/index.html', contents)


if __name__ == '__main__':
    main()
